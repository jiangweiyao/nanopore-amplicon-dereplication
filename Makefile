install-miniconda:
	wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
	sh ./Miniconda3-latest-Linux-x86_64.sh -b -u
	PATH=~/miniconda3/bin:${PATH} conda init bash
	PATH=~/miniconda3/bin:${PATH} conda config --set auto_activate_base false
	rm ./Miniconda3-latest-Linux-x86_64.sh

install-geneflow:
	conda create -p ./geneflow --copy -y python=3
	source $$(conda info --base)/etc/profile.d/conda.sh; \
		conda activate ./geneflow; \
		pip install geneflow

env-file:
	conda env create -p ./env -f ./environment.yml

env:
	conda create -p ./env --copy -y -c bioconda -c conda-forge canu=1.9 medaka=0.11.5

clean: FORCE
	rm -rf ./env
	rm -rf ./geneflow

FORCE:
